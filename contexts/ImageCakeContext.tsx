import React, {ReactElement} from 'react';
import Modal, {DefaultModalContentsCover} from "../components/Modal";
const ImageCakeContext = React.createContext({});




const ImageContainer = function({children, closer}){
    return (
        <div className='image-viewer-container'>
            <style jsx>{`
                .image-viewer-container {
                    
                }
                
                .header {
                    background-color:#000;
                    color:#fff;
                    position:relative;
                }   
                
                
                .close {
                    position:absolute;
                    right:10px;
                    top:5px;
                    font-size:24px;
                }
            `}</style>
            <div className={'header'}>
                이미지 뷰

                <div className={'close'} onClick={closer}>
                    <i className={'fa fa-times'}/>
                </div>
            </div>
            {children}
        </div>
    )
}

type ImageType = {
    instance: ReactElement;
    url: string;
    group: string;
}
class ImageCakeProvider extends React.Component<
    {},
    {
        images: Array<ImageType>;
        lookTargetIndex: number | null;
        groupCursor: number;
    }
> {
    constructor(props) {
        super(props);


        this.state = {
            images: [],
            lookTargetIndex: null,
            groupCursor:0
        }
    }

    register = (reactInstance: ReactElement, imageUrl: string, group: string) => {
        this.setState((state, props) => ({
            images: [
                ...state.images,
                {
                    instance: reactInstance,
                    url: imageUrl,
                    group,
                }
            ],
        }))
    }


    unregister = (reactInstance: ReactElement) => {
        this.setState((state, props) => ({
            images: state.images.filter((image) => image.instance !== reactInstance ),
        }))
    }

    look = (instance) => {
        const targetInstanceIdx = this.state.images.findIndex((image) => image.instance === instance );
        if( targetInstanceIdx > -1 ){
            const targetInstance = this.state.images[targetInstanceIdx];
            if( targetInstance.group ){

                this.setState({
                    lookTargetIndex: targetInstanceIdx,
                    groupCursor: 0,
                })
            } else {
                this.setState({
                    lookTargetIndex: targetInstanceIdx,
                    groupCursor: null,
                })
            }
        }
    }

    renderViewer(){
        let lookingImage = this.state.images[this.state.lookTargetIndex];
        let groupMembers;
        if( lookingImage.group ){
            groupMembers = this.state.images.filter(( image ) => image.group === lookingImage.group )
        }


        return (
            <Modal
                contentsCover={ImageContainer}
                component={() => <img src={lookingImage.url} style={{maxWidth:'100%'}}/>}
                enableCloseByOutside

                props={{}}
                title={'image'}
                closer={() => this.setState({lookTargetIndex:null})}/>
        )
    }

    render(){
        const actions = {
            register: this.register,
            unregister: this.unregister,
            look: this.look,
        }

        return (
            <ImageCakeContext.Provider value={{actions}}>
                <style jsx>{`
                `}</style>
                <div>
                    {this.props.children}

                    { this.state.lookTargetIndex !== null && this.renderViewer() }
                </div>
            </ImageCakeContext.Provider>
        )
    }
}

export {
    ImageCakeContext,
    ImageCakeProvider
}