import React, {CSSProperties, HTMLAttributes, ReactComponentElement, StyleHTMLAttributes} from 'react';
import uuid from 'uuid';
import Modal, {DefaultModalContentsCover} from '../components/Modal';

const PlanetShaperContext = React.createContext({});
// const PlanetShaperProvider = PlanetShaperContext.Provider;
const PlanetShaperConsumer = PlanetShaperContext.Consumer;
PlanetShaperContext.displayName = 'PlanetShaperContext';


const TooltipBaseZIndex = 10000;

interface ModalOptionsType {
    contentsCover?:any;
    component: any;
    width?: number | string;
    height?: number | string;
    props?: object;
    title?: any;
}

interface TooltipOptionsType {

    contentsWrappingRect: {
        left: number;
        top: number;
        width: number;
        height: number;
    },

    tooltipContents: any;


    offsetVertical?: number;
    offsetHorizontal?: number;
}

interface TooltipItemType extends TooltipOptionsType{
    id: string;
}


interface ModalItemType extends ModalOptionsType{
    id: string;
}

interface ProviderStates {
    tooltips : Array<TooltipItemType>;
    modals: Array<ModalItemType>;
}





class FloatingTooltip extends React.Component
<{
    baseRect: {
        left: number;
        top: number;
        width: number;
        height: number;
    };

    contents:any;

    offsetVertical?: number;
    offsetHorizontal?: number;
},{
    ready: boolean;
    contentsWidth: number;
    contentsHeight: number;
}> {
    static defaultProps = {
        offsetHorizontal: 0,
        offsetVertical: 0,
    }

    dom:HTMLElement = null;
    constructor(props){
        super(props)

        this.state = {
            ready: false,
            contentsWidth: 0,
            contentsHeight: 0,
        }
    }

    componentDidMount(): void {
        const rect = this.dom.getBoundingClientRect();
        this.setState({
            contentsHeight: rect.height,
            contentsWidth: rect.width,
            ready: true,
        });
    }

    render(){
        let style: CSSProperties = {
            top: 0,
            left: 0,
        };

        style.top = this.props.baseRect.top + this.props.offsetVertical + 5;
        style.left = this.props.baseRect.left + this.props.offsetHorizontal;

        if( !this.state.ready ){
            style.visibility = "hidden";
        } else {
            style.opacity = 1;
            style.top -= 5;
        }

        return (
            <div className='planet-shaper-tooltip' ref={(dom) => this.dom = dom} style={style} >
                <style jsx>{`
                    .planet-shaper-tooltip {
                        position:fixed;
                        z-index:${TooltipBaseZIndex};
                        transition: left .3s, top .3s, opacity .3s;
                        opacity:0; 
                    }
                    
                   .planet-shaper-tooltip .contents-wrapper {
                        position:absolute;
                        bottom:0;
                        left:0;
                   }
                `}</style>

                <div className='contents-wrapper'>
                    {this.props.contents}
                </div>
            </div>
        )
    }
}

class PlanetShaperProvider extends React.Component<{},ProviderStates> {
    constructor(props) {
        super(props);

        this.state = {
            tooltips: [],
            modals: []
        }
    }

    openTooltip = (id : string, options : TooltipOptionsType) => {
        console.log('openTooltip');

        this.setState({
            tooltips: [
                ...this.state.tooltips,
                {
                    id,
                    ...options,
                }
            ]
        })
    }

    closeTooltip = (id:string) => {
        this.setState({
            tooltips: this.state.tooltips.filter( (ti) => ti.id !== id )
        })
    }

    openModal = (options : ModalOptionsType, id : string = uuid.v4()) => {
        console.log('openModal');


        this.setState({
            modals: [
                ...this.state.modals,
                {
                    id,
                    ...options,
                }
            ]
        })

        return id;
    }

    closeModal = (id:string) => {
        this.setState({
            modals: this.state.modals.filter( (ti) => ti.id !== id )
        })
    }

    render(){
        const actions = {
            openTooltip : this.openTooltip,
            closeTooltip: this.closeTooltip,
            openModal: this.openModal,
            closeModal: this.closeModal,
        }

        return (
            <PlanetShaperContext.Provider value={{actions}}>
                <div>
                    {this.props.children}

                    <div>
                        {this.state.modals.map((params: ModalItemType) => (
                            <Modal
                                key={params.id}
                                component={params.component}
                                contentsCover={params.contentsCover || DefaultModalContentsCover}
                                height={params.height}
                                width={params.width}
                                props={params.props || {}}
                                title={params.title || ''}
                                closer={() => this.closeModal(params.id)}
                            />)
                        )}
                    </div>

                    <div>
                        {this.state.tooltips.map((params: TooltipItemType) => (
                            <FloatingTooltip
                                key={params.id}
                                baseRect={params.contentsWrappingRect}
                                contents={params.tooltipContents}
                                offsetHorizontal={params.offsetHorizontal}
                                offsetVertical={params.offsetVertical}
                            />)
                        )}
                    </div>
                </div>
            </PlanetShaperContext.Provider>
        )
    }
}


export {
    PlanetShaperContext,
    PlanetShaperProvider,
    PlanetShaperConsumer
}