export type RentDocumentType = "rent" | "return";
export type SurfaceTypes = "front" | "leftFront" | "rightFront" | "leftSide" | "rightSide" | "rear" | "leftRear" | "rightRear"
export interface ScanResult {
    seq: number;
    finishDateTime:string;
    direction: SurfaceTypes;
    fileIndex: number;
    damage1: number;
    damage2: number;
    damage3: number;
    damage4: number;
    damage5: number;
    damage6: number;
    damage7: number;
    damage8: number;
    damage9: number;
    damage10: number;
    damage11: number;
}

export type  PartReport = {
    damageMap: Map<number, Array<ScanResult>>;
    // damageMap2?: Object<string, Array<ScanResult>>;
    hasDamage: boolean;
    rawPartScans: Array<ScanResult>;
}

export type CarItem = {
    seq : number;
    carNo: string;
}

export type RentDocument = {
    seq: number;
    carSeq: number;
    action: RentDocumentType;
    scanResult?: Array<ScanResult>;
    lat: string;
    lng: string;
}