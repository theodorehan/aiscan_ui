
if( process.env.CONFIG === 'dev' ){
    module.exports = require('./dev')
} else {
    module.exports = require('./local')
}
