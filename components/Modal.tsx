import React from "react";

const ModalBaseZIndex = 5000;
export default class Modal extends React.Component
    <{
        contentsCover: any;
        component: any;
        width?: number | string;
        height?: number | string;
        props: object;
        title: any;
        closer: () => void;
        enableCloseByOutside?: boolean;
    }>
{

    constructor(props) {
        super(props);

    }

    clickDim = () => {
        if( this.props.enableCloseByOutside ){
            this.props.closer();
        }
    }


    render(){
        const Comp = this.props.component;
        const ContentsCover = this.props.contentsCover;
        const {props, title, closer} = this.props;

        return (
            <div className='modal-wrap'>
                <style jsx>{`
                    .modal-wrap {
                        left:0;
                        top:0;
                        position:absolute;
                        min-width:100%;
                        min-height:100%; 
                        z-index:${ModalBaseZIndex};
                        text-align:center; 
                    }
                    
                     .dim {
                        left:0;
                        top:0;
                        position:fixed;
                        min-width:100%;
                        min-height:100%;
                        background-color:rgba(0,0,0,0.4); 
                        z-index:1;
                    }
                    
                    .modal-wrap:before {
                    
                        display:inline-block;
                        content: " ";
                        min-height:100vh;
                        width:0;
                        vertical-align: middle;
                        
                    }
                    
                    .modal-vertical-aligner {
                        display:inline-block;
                        vertical-align:middle;
                         
                    }
                    
                    .modal-contents-wrapper {
                        position:relative;
                        z-index:2;
                    }
                `}</style>

                <div className='dim' onClick={this.clickDim}></div>

                <div className='modal-vertical-aligner'>
                    <div className='modal-contents-wrapper'>
                        <ContentsCover title={title} closer={closer} style={{width:this.props.width, height: this.props.height}} >
                            <Comp {...props}/>
                        </ContentsCover>
                    </div>
                </div>
            </div>
        )
    }
}


export const DefaultModalContentsCover = ({closer, children, title, style}) => (
    <div className='default-contents-cover' style={style}>
        <style jsx>{`
            .default-contents-cover {
                width:100%;
                height:100%;
                background-color:#fff;
                border-radius:10px;
                position:relative;
            }
            
            .title {
                background-color: #3095fb;
                border-radius: 10px 10px 0 0;
                color: #fff;
                height: 35px;
                font-size: 20px;
                padding: 5px;
                box-sizing: border-box;
                
            }
            
            .close {
                position: absolute;
                right: 10px;
                top: 7px;
                cursor:pointer;
            }
        `}</style>
        { title && (
            <div className='title'>
                { title }
                <div className='close' onClick={closer}>
                    <i className='fa fa-times'/>
                </div>
            </div>
        )}

        {children}
    </div>
)