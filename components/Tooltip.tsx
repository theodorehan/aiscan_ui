import React from 'react';
import {findDOMNode} from 'react-dom';
import uuid from 'uuid';
import {PlanetShaperContext} from "../contexts/PlanetShaperProvider";



export const TooltipBoxContainer = ({children}) => (
    <div className='tooltip-box-container'>
        <style jsx>{`
            .tooltip-box-container {
                background-color:rgba(0,0,0,0.7);
                padding:10px;
                
                word-break: keep-all;
                white-space: nowrap;
            }
        `}</style>

        {children}
    </div>
)


export default class Tooltip extends React.Component<
    {
        disable?: boolean;
        tooltipContents : any;
        tooltipBoxContainer?: any;

        offsetVertical?: number;
        offsetHorizontal?: number;
    }
> {
    static defaultProps = {
        tooltipBoxContainer: TooltipBoxContainer
    }

    static contextType = PlanetShaperContext;
    $dom:HTMLElement = null;
    id: string;

    constructor(props) {
        super(props);

        this.state = {
            show:false,
            left:0,
            top:0,
        }

        this.id = uuid.v4();
    }

    mouseOverContent = () => {
        const baseRect = this.$dom.getBoundingClientRect();

        this.context.actions.openTooltip(this.id, {

            contentsWrappingRect: {
                left: baseRect.left,
                top: baseRect.top,
                width: baseRect.width,
                height: baseRect.height,
            },
            tooltipContents: (
                <this.props.tooltipBoxContainer>
                    {this.props.tooltipContents}
                </this.props.tooltipBoxContainer>
            ),


            offsetVertical: this.props.offsetVertical,
            offsetHorizontal: this.props.offsetHorizontal,
        });
    }

    mouseOutContent = () => {
        this.context.actions.closeTooltip(this.id);
    }


    bindEvents(){

        const dom = findDOMNode(this);
        this.$dom = dom;
        if( dom ){

            // remove for prevent to be duplicated events
            dom.removeEventListener('mouseover', this.mouseOverContent);
            dom.removeEventListener('mouseout', this.mouseOutContent);

            if( this.props.disable ) return;
            // bind
            dom.addEventListener('mouseover', this.mouseOverContent);
            dom.addEventListener('mouseout', this.mouseOutContent);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.bindEvents();
    }

    componentDidMount() {
        this.bindEvents();
        console.log(findDOMNode(this));
    }

    render(){
        return (
            <>
                {this.props.children}
            </>
        )

    }
}

