import React from 'react';


const defaultPager = ({children, isCurrent}) => (
    <div className={'pager ' + (isCurrent && 'cur')}>
        <style jsx>{`
            .pager {
                display:inline-block;
                
                background-color:#fff;
                border-radius:10px;
                padding:3px 5px;
                cursor:pointer;
                margin:3px;
            }
            
            .cur {
            
                background-color:#fff;
                box-shadow: 0 0 10px 0 #fff;
            }
        `}</style>

        {children}
    </div>
)


class Pagenation extends React.Component<
    {
        pageCount : number;
        currentPage: number;
        pageMarkableCount: number;
        pageStartZero?: boolean;
        PagerRender?: any;
        onMovePage?:(page:number) => void;
    }
> {
    static defaultProps = {
        PagerRender: defaultPager,
    }
    constructor(props){
        super(props)
    }

    clickPage(page:number){
        if( this.props.onMovePage ){
            this.props.onMovePage(page);
        }
    }

    render(){
        let currentCursorRange = Math.floor(this.props.currentPage / this.props.pageMarkableCount);
        if( !this.props.pageStartZero ){

        }

        let haveLeftArrow = false;
        let haveRightArrow = false;
        const pages = [];
        for(let i = 0; i < this.props.pageMarkableCount; i++ ){
            pages.push( currentCursorRange * this.props.pageMarkableCount + ( this.props.pageStartZero ? 0 : 1 ) + i );
        }

        if( currentCursorRange < (this.props.pageCount / this.props.pageMarkableCount) ){
            haveRightArrow = true;
        }

        if( currentCursorRange > 0 ){
            haveLeftArrow = true;
        }

        return (
            <ul>
                <style jsx>{`
                    ul {
                         display:inline-block;
                    }
                    
                    li {
                        display:inline-block;
                    }
                `}</style>
                {
                    haveLeftArrow && (
                        <li onClick={() => this.clickPage(Math.max(this.props.currentPage - this.props.pageMarkableCount, 0))}>
                            <this.props.PagerRender>
                                <i className='fa fa-arrow-left'/>
                            </this.props.PagerRender>
                        </li>
                    )
                }

                {
                    pages.map((page, i) => (
                        <li key={i} onClick={() => this.clickPage(page)}>
                            <this.props.PagerRender isCurrent={this.props.currentPage === page}>
                                {page}
                            </this.props.PagerRender>
                        </li>
                    ))
                }

                {
                    haveRightArrow && (
                        <li onClick={() => this.clickPage(Math.min(this.props.currentPage + this.props.pageMarkableCount, this.props.pageCount))}>
                            <this.props.PagerRender>
                                <i className='fa fa-arrow-right'/>
                            </this.props.PagerRender>
                        </li>
                    )
                }
            </ul>
        )
    }
}

export default Pagenation;