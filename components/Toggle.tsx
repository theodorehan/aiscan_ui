import React from 'react';


interface TogglePropTypes {
    onToggle? : ( value: boolean ) => void;
    value? : boolean;
}

export default class Toggle extends React.Component
    <{
        onToggle? : ( value: boolean ) => void;
        value? : boolean;
    },{
        value: boolean;
    }>
{

    constructor(props){
        super(props);

        this.state = {
            value: props.value || false,
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if( nextProps.value !== undefined ){
            return {
                value: nextProps.value || false,
            };
        }
    }

    toggler = () => {

        console.log(this.state.value)
        this.setState({value: !this.state.value}, ( ) => {
            if( this.props.onToggle ){
                this.props.onToggle(this.state.value);
            }
        });


    }

    render(){
            return (
                <div className={'planet-shaper-toggle '+ ( this.state.value ? 'on' : '' )} onClick={ this.toggler }>
                    <style jsx>{`
                .planet-shaper-toggle {
                    display:inline-block;
                    width: 60px;
                    height: 30px;
                    border-radius: 57px;
                    /* border: 2px solid #fff; */ 
                    background-color: rgba(0, 0, 0, 0.25);
                    box-sizing: border-box;
                    position:relative;
                    transition:background-color .3s;
                }
                
                .planet-shaper-toggle.on {
                    background-color: #1890ff;
                }
                
                .circle {
                    background-color: #fff;
                    width: 26px;
                    height: 26px;
                    border-radius: 15px;
                    border: 1px solid #fff;
                    position:absolute;
                    
                    left:2px;
                    top:2px;
                    transition:left .3s;
                    outline:none;
                }
                
                .planet-shaper-toggle.on .circle {
                    left:30px;
                }
            `}</style>

                    <button className={'circle ' + 'on'} />
                </div>
            )
    }

}