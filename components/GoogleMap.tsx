import React, {DOMElement} from 'react';
import PropTypes from 'prop-types';

import _ from 'lodash'
import Promise from 'bluebird';
import ClickOutside from './ClickOutside';

declare var google:any;

type PlaceInfo = {
    lat: string;
    lng: string;
    placeId: string;
};

// export class GoogleMapProvider extends React.Component {
//     getGoogleMaps(callback){
//         const apiKey = this.props.apiKey;
//         const scriptID = 'google-map-api-starter-script';
//         const scriptName = 'googleMapInit';
//
//         const existsDom = document.querySelector('#'+scriptID )
//         if( existsDom && typeof google !== 'undefined' && google.maps ){
//             callback( google.maps );
//         } else if(!existsDom) {
//
//             window[scriptName] = function ( ) {
//                 callback( google.maps );
//             }
//
//             const scriptDom = document.createElement('script');
//             scriptDom.setAttribute('src', `https://maps.googleapis.com/maps/api/js?key=${apiKey}&libraries=places&callback=${scriptName}`);
//             scriptDom.setAttribute('id', scriptID);
//             document.head.appendChild(scriptDom);
//         } else {
//             // interval
//         }
//
//     }
//
//     componentDidMount(){
//
//     }
//
//
// }

export default class GoogleMap extends React.Component<
    {
        apiVersion?: string;
        onApply?: ( PlaceInfo, any  ) => void;
        startAt?: string;
        apiKey?: string;
        search?: boolean;
        closeSelf?: () => void;
        width?: string | number;
        height?: string | number;
    },{
        currentInfo : PlaceInfo;
        spreading: boolean;

        itemCursor: number;
        items: Array<any>;
    }
>{
    getGoogleMaps(callback){
        const apiKey = this.props.apiKey;
        const scriptID = 'google-map-api-starter-script';
        const scriptName = 'googleMapInit';

        const existsDom = document.querySelector('#'+scriptID )
        if( existsDom && typeof google !== 'undefined' && google.maps ){
            callback( google.maps );
        } else if(!existsDom) {

            window[scriptName] = function ( ) {
                callback( google.maps );
            }

            const scriptDom = document.createElement('script');
            scriptDom.setAttribute('src', `https://maps.googleapis.com/maps/api/js?key=${apiKey}&libraries=places&callback=${scriptName}`);
            scriptDom.setAttribute('id', scriptID);
            document.head.appendChild(scriptDom);
        } else {
            // interval
        }

    }

    dom : HTMLElement;
    wrapper : HTMLElement;
    locateMarker: any;
    marker: any = null;
    map : any = null;
    placeService : any = null;

    constructor( props ) {
        super( props );



        this.map = null;
        this.placeService = null;

        this.state = {
            items : [],
            spreading: false,
            itemCursor: 0,
            currentInfo: {
                lat:null,
                lng:null,
                placeId:null,
            },
        }
    }

    applyCurrent = () => {
        if( this.props.onApply ){
            if( this.state.currentInfo.lat && this.state.currentInfo.lng ){

                this.props.onApply(this.state.currentInfo, this.props.closeSelf);
            } else {
                alert('선택된 위치가 없습니다.');
            }
        }
    }

    cursorDown(){
        let next = this.state.itemCursor + 1;
        if( this.state.items.length <= next ){
            next = 0;
        }

        console.log('cursor', next);
        this.setState({
            itemCursor:next
        })
    }

    cursorUp(){
        let next = this.state.itemCursor - 1;
        if( 0 > next ){
            next = this.state.items.length-1;
        }

        console.log('cursor', next);
        this.setState({
            itemCursor:next
        })
    }

    setMapCenter(location){
        // console.log(lat, lng);
        this.map.setCenter(location);
        this.map.setZoom(17);
    }

    nearbySearch(location){
        return new Promise((resolve, reject) => {

            this.placeService.nearbySearch({
                location:location,
                radius: 500,
            }, (result, status) => {

                resolve(result);
            })
        })
    }

    getDetailPlace(placeId){
        return new Promise((resolve,reject) => {
            this.placeService.getDetails({
                placeId,
                fields: ['ALL'],
            }, (result, status) => {
                resolve(result);
            })
        })
    }

    async setCurrent(location, placeId?){



        if( !placeId ){

            let nearbyResult = await this.nearbySearch(location);
            console.log('nearbyResult',nearbyResult);

            if( nearbyResult ){
                const first = nearbyResult[0];

                console.log(first);
                if( first  ){
                    let detail = await this.getDetailPlace(first.place_id);
                    console.log('detail',detail);
                }
            }



        } else {

            let detail = await this.getDetailPlace(placeId);

            console.log('detail',detail)
        }


        this.setMainMarker(location);
        this.setState({
            currentInfo: {
                lat: location.lat(),
                lng: location.lng(),
                placeId: placeId,
            }
        })
    }

    cursorApply(index){
        if( this.state.items.length === 0 ) return;

        const item = this.state.items[index];

        if( !item ) return alert("Something went wrong");

        const loc = item.geometry.location;
        const lat = loc.lat();
        const lng = loc.lng();

        console.log(item);
        this.setState({ itemCursor: index, spreading: false });

        this.setMapCenter(loc);
        this.setCurrent(loc);
    }

    searchInputKeyup = (e) => {
        const ne = e.nativeEvent;
        const target = ne.target;
        const value = target.value;


        const keyCode = ne.keyCode;

        if( keyCode === 40 ){
            // down
            return this.cursorDown();
        } else if ( keyCode === 38 ){
            // up
            return this.cursorUp();
        } else if ( keyCode === 13 ){

            return this.cursorApply(this.state.itemCursor);
        }

        this.realTimeSearch(value);
    }

    realTimeSearch = _.debounce(async (text) => {
        console.log(text);

        this.placeService.textSearch({
            query:text
        }, (results,status) =>{
            console.log(results);
            this.setState({
                items: results,
                spreading: true,
                itemCursor: 0,
            });
        });


        console.log(this.placeService);
    }, 50)


    closeSearchResult = (e) => {
        this.setState({
            spreading: false,
        })
    }

    clickOnMap = (event) => {
        console.log('You clicked on: ' + event.latLng, event);

        // If the event has a placeId, use it.
        if (event.placeId) {

            console.log('You clicked on place:' + event.placeId);

            // Calling e.stop() on the event prevents the default info window from
            // showing.
            // If you call stop here when there is no placeId you will prevent some
            // other map click event handlers from receiving the event.
            event.stop();
            // this.calculateAndDisplayRoute(event.placeId);
            // this.getPlaceInformation(event.placeId);
        }

        this.setCurrent(event.latLng, event.placeId);
        // this.setCurrent(event.latLng);
    };

    setMainMarker(myLatLng){
        if( this.marker ){
            this.marker.setMap(null);
            this.marker = null;
        }

        this.marker = new google.maps.Marker({
            position: myLatLng,
            map: this.map,
            title: 'Hello World!'
        });
    }

    updateStartPoint(){
        if( this.map ){
            const pair = this.props.startAt.split(',');
            const lat = pair[0];
            const lng = pair[1];

            if( lat && lng ){
                let latLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                this.setCurrent(latLng);
                this.setMapCenter(latLng);
            }
        }
    }

    componentDidUpdate(prevProps){
        if( this.props.startAt !== prevProps.startAt ){
            console.log(this.props.startAt, prevProps.startAt)
            this.updateStartPoint();
        }
    }

    componentDidMount() {


        this.getGoogleMaps((maps) => {


            this.wrapper = this.dom.querySelector('.google-map-wrapper');

            const map = new maps.Map(this.wrapper, {
                center: {lat: 37.566535, lng: 126.97796919999996},
                zoom: 8,
                styles: [
                    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                    {
                        featureType: 'administrative.locality',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#d59563'}]
                    },
                    {
                        featureType: 'poi',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#d59563'}]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'geometry',
                        stylers: [{color: '#263c3f'}]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#6b9a76'}]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry',
                        stylers: [{color: '#38414e'}]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry.stroke',
                        stylers: [{color: '#212a37'}]
                    },
                    {
                        featureType: 'road',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#9ca5b3'}]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [{color: '#746855'}]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry.stroke',
                        stylers: [{color: '#1f2835'}]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#f3d19c'}]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'geometry',
                        stylers: [{color: '#2f3948'}]
                    },
                    {
                        featureType: 'transit.station',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#d59563'}]
                    },
                    {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [{color: '#17263c'}]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#515c6d'}]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.stroke',
                        stylers: [{color: '#17263c'}]
                    }
                ]
            });
            this.map = map;

            this.placeService = new google.maps.places.PlacesService(map);

            this.map.addListener('click', this.clickOnMap);

            // initialize selected location
            if( this.props.startAt ){
                this.updateStartPoint()
            }

            this.locateMarker = null;
        })

    }

    renderSearch(){
        return (
            <ClickOutside onClickOutside={this.closeSearchResult}>

                <style jsx>{`
     
          
          .search_field {
            position:absolute;
            z-index:2;
            
            width: 400px;
            left: 50%;
            top: 20px;
            margin-left: -200px;
            background-color: rgba(255,255,255,0.6);
          
            border-radius: 10px;
            box-shadow: 0 0 20px 0 #0000004a;
            transition:background-color .3s;
          }
          
          .search_field:hover {
            background-color: rgba(255,255,255,0.8);
          }
          
          .search_field input {
            border:0;
            font-size:20px;
            width:100%;
            padding: 5px 25px;
            outline:0;
            background-color:transparent;
          }
           
        `}</style>


                <div className='search_field'>
                    {/*<Autocomplete/>*/}
                    <div className='search-icon'>
                        S
                    </div>
                    <input onKeyUp={this.searchInputKeyup}/>

                    {
                        (this.state.spreading && this.state.items && this.state.items.length > 0) ? (
                            <div>
                                <ul>
                                    { this.state.items.slice(0,10).map((item, idx) => (
                                        <li className={ idx === this.state.itemCursor && 'focus' } key={item.geometry.location.toString()} onClick={() => this.cursorApply(idx)}>
                                            <div className='name'>
                          <span className='icon'>
                            <img src={item.icon} style={{width:15}}/>
                          </span>

                                                {item.name}
                                            </div>

                                            <div className={'sub'}>
                                                { item.formatted_address }
                                            </div>
                                        </li>
                                    ))}

                                </ul>
                            </div>
                        ):null
                    }
                </div>
            </ClickOutside>
        )
    }

    render(){
        return (
            <div className='google-map-provider' style={{width: this.props.width || '100%', height:this.props.height || '100%'}}>
                <style jsx>{`
        
          .google-map-provider {
            position:relative;
          }
          
          .detail-panel {
            background-color: #ffffffad;
            // border-radius: 5px;
            position: absolute;
            box-shadow: 0 0 20px 0 #0000004a;
            max-width: 300px;
            font-size: 14px;
            z-index: 2;
            right: 10px;
            top: 60px;
            min-height: 40px;
          }
          
          .detail-panel .content {
            
          }
          
          .search_field {
            position:absolute;
            z-index:2;
            
            width: 400px;
            left: 50%;
            top: 20px;
            margin-left: -200px;
            background-color: rgba(255,255,255,0.6);
          
            border-radius: 10px;
            box-shadow: 0 0 20px 0 #0000004a;
            transition:background-color .3s;
          }
          
          .search_field:hover {
            background-color: rgba(255,255,255,0.8);
          }
          
          .search_field input {
            border:0;
            font-size:20px;
            width:100%;
            padding: 5px 25px;
            outline:0;
            background-color:transparent;
          }
          
          .search-icon {
            position:absolute;
            left:5px;
            top:7px;
            font-size:18px;
          }
          
          ul {
            list-style-type: none;
            margin:0;
            padding:0;
            width:100%;
          }
          
          li {
            margin:0;
            padding:5px 10px;
            width:100%;
            cursor:pointer;
          }
          
          li:hover {
            background-color:#cee0ef80;
          }
          
          li.focus {
          
            background-color:#cee0ef80;
          }
          
          
          li .title {
            
          }
          
          
          li .title .icon {
            width:20px;
            display:inline-block;
          }
          
          li .sub {
            font-size:10px;
          }
          
          button.apply {
            padding:5px 10px;
            background-color:#fff;
            border:0;
            margin:10px;
            background-color: #2b80ff;
            color: #fff;
          }
        `}</style>

                { this.props.search && this.renderSearch() }

                <div className='detail-panel'>
                    <div className={'content'}>
                        { this.props.onApply && ( <button className='apply' onClick={this.applyCurrent}> APPLY </button> )}
                    </div>

                </div>

                <div
                    style={{width:'100%', height:'100%'}}
                    ref={(dom) => this.dom = dom}
                    dangerouslySetInnerHTML={{__html:'<div style="width:100%;height:100%;" class="google-map-wrapper"></div>'}}/>
            </div>
        )
    }
}
