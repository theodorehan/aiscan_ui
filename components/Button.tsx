import React from 'react';

interface ButtonPropTypes {
    children: any;
    level?: "";
    onClick?: () => void;
}
export default function PlanetShaperButton({children, level, onClick} : ButtonPropTypes){
    return (
        <div className={'planet-shaper-button '+ ( level  )} onClick={ onClick }>
            <style jsx>{`
                .planet-shaper-button {
                    display: inline-block; 
                    height: 30px;
                    border-radius: 57px;
                    padding: 5px 20px;
                    color: #fff;
                    background-color: rgb(37, 111, 227); 
                    box-sizing: border-box;
                    position: relative;
                    -webkit-transition: background-color .3s;
                    transition: background-color .3s;
                    cursor:pointer;
                }
                   
            `}</style>

            {children}
        </div>
    )
}
