import React from 'react';
import PropTypes from 'prop-types';

export default class ClickOutside extends React.Component {

    static propTypes = {
        onClickOutside : PropTypes.func,
    }

    constructor(props){
        super(props)
    }

    componentDidMount() {
        window.addEventListener('click', this.listener);

    }

    componentWillUnmount() {
        window.removeEventListener('click', this.listener);
    }

    listener = (e) => {
        // console.log(e);
        let target = e.target;


        let isOutside = true;
        while( target.parentElement ){
            target = target.parentElement;
            if( this.rootDom === target ){
                isOutside = false;
            }
        }

        if( isOutside ){
            if( this.props.onClickOutside ){
                this.props.onClickOutside(e);
            }
        }
    }


    render(){
        return (
            <div ref={(dom) => this.rootDom = dom} >
                {this.props.children}
            </div>
        )
    }
}
