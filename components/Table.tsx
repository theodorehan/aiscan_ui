import React from 'react';
import css from './Table.module.css';
interface ColumnFeatures {
    expand?: (rowIdx: number) => void;
}

type ColumnDefine = {
    key: string | '$expand';
    title: any;
    render?: (row: {[key:string]:any}, key: string, rowIndex:number) => any;
    colSpan? : (row: {[key:string]:any}, key: string, rowIndex:number) => number;
    rowSpan? : (row: {[key:string]:any}, key: string, rowIndex:number) => number;
    skipTest? : (row: {[key:string]:any}, key: string, rowIndex:number) => boolean;
    width?: number | string;
}

export default class Table<T> extends React.Component
<
    {
        columns: Array<ColumnDefine>;
        theme?:string;

        rows: Array<any>;
        renderExpansion ? : (row: any, index?: number) => any;

        horizontalTable?: boolean;
        outsideBorder?: boolean;
        numbering?: boolean;

        onClickRow?: (row?:any, index?:number) => void;
    },
    {
        expanded: number | null; //  Expanded Row Index
    }
>
{
    constructor(props)
    {
        super(props);

        this.state = {
            expanded: null,
        }
    }

    getRows()
    {
        return this.props.rows;
    }

    onRowExpand = (rowIdx: number) => {
        console.log('expand', rowIdx)

        if( this.state.expanded === rowIdx ){
            this.setState({
                expanded: null,
            })
        } else {
            this.setState({
                expanded: rowIdx,
            })
        }

    }

    columnFeatures()
:
    ColumnFeatures
    {
        return {
            expand: this.onRowExpand,
        }
    }

    colSpan=  (row: {[key:string]:any}, key: string, rowIndex:number, column: ColumnDefine)  => {
        if( this.props.horizontalTable ){
            if( column.rowSpan ){
                return column.rowSpan(row, key, rowIndex);
            }
        } else {
            if( column.colSpan ){
                return column.colSpan(row, key, rowIndex);
            }
        }


        return 1;
    }

    rowSpan=  (row: {[key:string]:any}, key: string, rowIndex:number, column: ColumnDefine)  => {
        if( this.props.horizontalTable ){
            if( column.colSpan ){
                return column.colSpan(row, key, rowIndex);
            }
        } else {
            if( column.rowSpan ){
                return column.rowSpan(row, key, rowIndex);
            }
        }


        return 1;
    }

    renderHorizontalRows(){
        return this.props.columns.map((col) => (
            <tr className={'row' + ' ' + css.tr} key={col.key}>

                <td
                    className={css.headerColumn + ' ' + css.tableData} key='column'>
                    {col.title}
                </td>

                {
                    this.props.rows.map((row,i) => this.renderColumn(row,col,i)).filter((colElement) => colElement !== null )
                }
            </tr>
        ))
    }

    renderColumn(row, col, rowIndex)
    {
        if( col.skipTest && col.skipTest(row, col.key, rowIndex) ){
            return null;
        }

        if (col.render) {
            return (
                <td
                    className={css.tableData} key={col.key}
                    colSpan={this.colSpan(row, col.key, rowIndex, col)}
                    rowSpan={this.rowSpan(row, col.key, rowIndex, col)}>
                    {col.render(row, col.key, this.columnFeatures())}
                </td>
            )
        }

        if (col.key === '$expand') {
            return (
                <td className={css.tableData} key='$expand' >
                    <button className={css.expandButton}  onClick={() => this.onRowExpand(rowIndex)}>+</button>
                </td>
            )
        }

        return (
            <td
                key={col.key}
                className={css.tableData}
                colSpan={this.colSpan(row, col.key, rowIndex, col)}
                rowSpan={this.rowSpan(row, col.key, rowIndex, col)}>

                {row[col.key]}
            </td>
        )
    }

    renderRow(row, index)
    {
        return (
            <React.Fragment key={index}>
                <style jsx>{`
                    tr{
                    
                        ${ this.props.onClickRow && 'cursor:pointer;'}
                    }
                `}</style>
                <tr className={css.tr} key={index +'-data' } onClick={() => this.props.onClickRow && this.props.onClickRow(row, index)}>
                    {
                        this.props.columns.map((col) => this.renderColumn(row, col, index)).filter((colElement) => colElement !== null )
                    }
                </tr>
                {
                    this.state.expanded === index && (
                        <tr className={css.tr} key={index + '-expanded'}>
                            <td className={css.tableData} colSpan={this.props.columns.length}>
                                { this.props.renderExpansion(row, index) }
                            </td>
                        </tr>
                    )
                }

            </React.Fragment>
        )
    }


    render()
    {
        return (
            <table className={'planet-shaper-table '+ this.props.theme}>
                <style jsx>{`
                    .planet-shaper-table {
                        width:100%;  
                        font-size: 10px;
                        ${ this.props.outsideBorder ? 'border:1px solid #bbb':''};
                    }
                    
                    .planet-shaper-table thead {
                        border-bottom: 1px solid #bbb;
                        font-size:12px;
                    }
                    
                    .planet-shaper-table thead th {
                      
                    }
                    
                    .planet-shaper-table thead th:last-child {
                  
                        border-right:0px; 
                    }
                    
                    .planet-shaper-table tbody tr {
                        border-bottom: 1px solid #bbb;
                    }
                    
                     .planet-shaper-table tbody tr:last-child {
                        border-bottom: 0;
                    }
                    
                    
                    .planet-shaper-table tbody tr td {
                        padding:5px;
                        border-right:1px solid #bbb;
                        color:#777;
                    }
                    
                    .planet-shaper-table tbody tr td:last-child {
                  
                        border-right:0px; 
                    }
                `}</style>

                <colgroup>
                    {
                        this.props.columns.map((col) => (
                            <col key={col.key} width={col.width || undefined}/>
                        ))
                    }
                </colgroup>

                <thead>
                    {
                        !this.props.horizontalTable && (
                            <tr className={css.tr}>
                                {
                                    this.props.columns.map((col) => (
                                        <th className={css.headerColumn} key={col.key}>
                                            {col.title}
                                        </th>
                                    ))
                                }
                            </tr>
                        )
                    }
                </thead>
                <tbody>
                    { this.props.horizontalTable ? (
                        this.renderHorizontalRows()
                    ) : (
                        this.getRows().map((row, i) => this.renderRow(row, i))
                    )}
                </tbody>
            </table>
        )
    }
}