import {CSSProperties, ReactElement, StyleHTMLAttributes} from "react";

interface LabelProps {
    children:any;
    onClick?: () => void;
    style?: CSSProperties;
}
export default function Label ({children, onClick, style} : LabelProps){
    return (
        <div className='label' onClick={onClick} style={style}>
            <style jsx>{`
            
                .label {
                    display: inline-block;
                    color: #fff;
                    border-radius: 20px;
                    padding: 5px 10px;
                    font-size: 14px;
                    background-color: #202020;
                }
            `}</style>

            {children}
        </div>
    )
}