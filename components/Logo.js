import React from 'react';
import css from './Logo.module.css';

export default (props) => (
    <div className={css.logoWrapper + ' ' + props.theme}>
        <style jsx>{`
            .dark {
                color:#333;
            }
        `}</style>
        <div className={css.logo}>AISCAN</div>
        <span className={css.sub}> monitoring system </span>
    </div>
)