import React from 'react';
import {ImageCakeContext} from "../contexts/ImageCakeContext";



export default class Image extends React.Component<
    {
        url: string;
        width?: number | string;
        height?: number | string;
        style?: object;
        group?: string;
        onClick?: (url?, group?) => void;
    },
    {

    }
> {

    constructor(props) {
        super(props);

    }

    static contextType = ImageCakeContext;


    lookingBigger = (e) => {
        e.stopPropagation();
        if( this.context.actions ){
            this.context.actions.look(this);
        }
    }

    componentDidMount(): void {


        if( this.context.actions ){
            this.context.actions.register(this, this.props.url);
        }
    }

    componentWillUnmount(): void {
        if( this.context.actions ){
            this.context.actions.unregister(this);
        }
    }

    render(){
        const { width, height } = this.props;

        return (
            <div className='planet-shaper-image' style={{width, height, ...this.props.style}} onClick={(e) => this.props.onClick(this.props.url, this.props.group)}>
                <style jsx>{`
                    .planet-shaper-image {
                        display:inline-block;
                    }
                    
                    img {
                        max-width:100%;
                    }
                    
                    .popup {
                    
                    }
                `}</style>

                <img src={this.props.url} onClick={this.lookingBigger}/>

                <div>

                </div>
            </div>
        )
    }
}