import React, {CSSProperties} from 'react';
import PropTypes from 'prop-types';

import Tooltip from "./Tooltip";
import {PartReport, ScanResult, SurfaceTypes} from "../@types/data";
import {DamageNumberMap} from "../constants/damage";

const WarnDot = ({level}) => (
    <div className={'warn-dot' + ' ' + level}>
        <style jsx>{`
            .warn-dot { 
                display:inline-block;
                background-color: #fff;
                border-radius: 20px;
                width: 5px;
                margin: 5px;
                height: 5px;
                box-shadow: 0 0 10px 0 #fff;
            }
            .warn-dot.green {
                background-color: #85ff00;
                box-shadow: 0 0 10px 0 #12ff45;
            }
            .warn-dot.red {
                background-color: #ff5e00; 
                box-shadow: 0 0 10px 0 #ff2f00;
            }
        `}</style>

    </div>
)

const PulsingDot = ({className, level, style, onClick, focused}) => (
    <div className={'pulse ' + className + ' ' + level + ' ' + (focused?'focused':'') } style={style} onClick={onClick}>
        <style jsx>{`
        .pulse {
 
  display: block;
  width: 15px;
  height: 15px;
  border-radius: 50%;
  background: #2580c0;
  cursor: pointer;
  box-shadow: 0 0 0 rgba(25, 162, 227, 0.79);
  animation: pulse 2s infinite; 
}
.pulse:hover {
  animation: pulse-highlight  2s infinite; 
}
 
.pulse.warn {
  background: #ddb112;
}

.pulse.focused {
    
      border: 2px solid #fffffff0;
  animation: pulse-focused  2s infinite; 
}

@keyframes pulse {
  0% { 
    box-shadow: 0 0 0 0 rgba(25, 162, 227, 0.79);
  }
  70% { 
      box-shadow: 0 0 0 10px rgba(25, 162, 227, 0);
  }
  100% { 
      box-shadow: 0 0 0 0 rgba(25, 162, 227, 0);
  }
}

@keyframes pulse-highlight {
  0% { 
    box-shadow:inset 0 0 7px #04f3ff, 0 0 0 0 rgb(19, 121, 209);
  }
  70% { 
      box-shadow:inset 0 0 7px #04f3ff, 0 0 0 10px rgba(25, 162, 227, 0);
  }
  100% { 
      box-shadow:inset 0 0 7px #04f3ff, 0 0 0 0 rgba(25, 162, 227, 0);
  }
}


@keyframes pulse-focused {
  0% { 
    box-shadow:inset 0 0 7px #04f3ff, 0 0 0 0 rgb(19, 121, 209);
  }
  70% { 
      box-shadow:inset 0 0 7px #04f3ff, 0 0 0 30px rgba(25, 162, 227, 0);
  }
  100% { 
      box-shadow:inset 0 0 7px #04f3ff, 0 0 0 0 rgba(25, 162, 227, 0);
  }
}



        `}</style>
    </div>
)

class PartResultViewer extends React.Component<{
    partReport: PartReport;
    title: string;

}> {
    constructor(props) {
        super(props)
    }

    renderDamage(damageNo, active) {
        return (
            <div>
                <WarnDot level={active ? 'red' : 'green'}/> {DamageNumberMap[damageNo]}
            </div>
        )
    }

    render() {
        return (
            <div className='part-short-report'>
                <style jsx>{`
                    
                    .part-short-report {
                        color:#fff;
                    }
                `}</style>
                <div className='name'> {this.props.title}</div>
                <div>
                    {this.renderDamage(1, !!this.props.partReport.damageMap.get(1))}
                    {this.renderDamage(2, !!this.props.partReport.damageMap.get(2))}
                    {this.renderDamage(3, !!this.props.partReport.damageMap.get(3))}
                    {this.renderDamage(4, !!this.props.partReport.damageMap.get(4))}
                    {this.renderDamage(5, !!this.props.partReport.damageMap.get(5))}
                    {this.renderDamage(6, !!this.props.partReport.damageMap.get(6))}
                    {this.renderDamage(7, !!this.props.partReport.damageMap.get(7))}
                    {this.renderDamage(8, !!this.props.partReport.damageMap.get(8))}
                    {this.renderDamage(9, !!this.props.partReport.damageMap.get(9))}
                    {this.renderDamage(10, !!this.props.partReport.damageMap.get(10))}
                    {this.renderDamage(11, !!this.props.partReport.damageMap.get(11))}
                </div>

            </div>
        )
    }
}

export default class VisualReport extends React.Component<{
    tooltip?:boolean;
    scanResults: Array<ScanResult>;
    onClickLocation?: ( location: SurfaceTypes, report: PartReport ) => void;
    focused?: SurfaceTypes;
    horizontal? : boolean;
}>{

    constructor(props) {
        super(props);


    }

    getPartResults(surface: SurfaceTypes): PartReport {
        function pushDamage(map, damageNo, item) {
            if (!Array.isArray(map.get(damageNo))) {
                map.set(damageNo,new Array());
            }
            map.get(damageNo).push(item);
        }

        const damageMap = new Map;
        let partScans = [];
        let hasDamage = false;
        if (this.props.scanResults) {
            const items = this.props.scanResults.filter((ret) => ret.direction === surface);
            partScans = items;

            let item;
            for (let i = 0; i < items.length; i++) {
                item = items[i];

                if (item.damage1 === 1) {
                    pushDamage(damageMap, 1, item);
                    hasDamage = true;
                }
                if (item.damage2 === 1) {
                    pushDamage(damageMap, 2, item);
                    hasDamage = true;
                }
                if (item.damage3 === 1) {
                    pushDamage(damageMap, 3, item);
                    hasDamage = true;
                }
                if (item.damage4 === 1) {
                    pushDamage(damageMap, 4, item);
                    hasDamage = true;
                }
                if (item.damage5 === 1) {
                    pushDamage(damageMap, 5, item);
                    hasDamage = true;
                }
                if (item.damage6 === 1) {
                    pushDamage(damageMap, 6, item);
                    hasDamage = true;
                }
                if (item.damage7 === 1) {
                    pushDamage(damageMap, 7, item);
                    hasDamage = true;
                }
                if (item.damage8 === 1) {
                    pushDamage(damageMap, 8, item);
                    hasDamage = true;
                }
                if (item.damage9 === 1) {
                    pushDamage(damageMap, 9, item);
                    hasDamage = true;
                }
                if (item.damage10 === 1) {
                    pushDamage(damageMap, 10, item);
                    hasDamage = true;
                }
                if (item.damage11 === 1) {
                    pushDamage(damageMap, 11, item);
                    hasDamage = true;
                }
            }
        }



        return {
            damageMap,
            hasDamage,
            rawPartScans : partScans,
        };
    }

    clickPart = ( part : SurfaceTypes) => {
        if( this.props.onClickLocation ){
            this.props.onClickLocation(part,  this.getPartResults(part));
        }
    }

    componentDidMount(): void {
        console.log(this.getPartResults("front"))
    }


    render() {
        const partResults : Map<SurfaceTypes, PartReport> = new Map();
        partResults.set("front", this.getPartResults('front'));
        partResults.set("rightFront", this.getPartResults('rightFront'));
        partResults.set("rightRear", this.getPartResults('rightRear'));
        partResults.set("rightSide", this.getPartResults('rightSide'));
        partResults.set("leftRear", this.getPartResults('leftRear'));
        partResults.set("leftSide", this.getPartResults('leftSide'));
        partResults.set("leftFront", this.getPartResults('leftFront'));
        partResults.set("rear", this.getPartResults('rear'));



        const styleSet : { [key:string]: CSSProperties} = {
            'front': {left: '50%'},
            'rightFront' : {left: '90%', top: '10%'},
            'rightSide' : {left: '100%', top: '50%'},
            'rightRear' : {left: '90%', top: '90%'},
            'leftFront' : {left: '10%', top: '10%'},
            'leftSide' : {left: '0', top: '50%'},
            'leftRear' : {left: '10%', top: '90%'},
            'rear' : {left: '50%', bottom: 0},
        }

        if( this.props.horizontal ){
            styleSet.front = { right: '-5%', top:'50%' }
            styleSet.rear = { left: '-5%', top:'50%' }

            styleSet.leftFront = { left: '100%', top:'15%' }
            styleSet.leftSide = { left: '50%', top:'10%' }
            styleSet.leftRear = { left: '0%', top:'15%' }

            styleSet.rightFront = { left: '100%', bottom:'12%' }
            styleSet.rightSide = { left:'50%', bottom:'6%' }
            styleSet.rightRear = { left: 0, bottom:'12%' }

        }





        return (
            <div className={ 'visual-report ' + (this.props.horizontal?'horizontal':'') }>
                <style>{`
                    .visual-report {
                       
                        display:inline-block;
                        
                    }
                    
                    .car-stage {
                        position:relative;
                        width:250px;
                        height:440px;
                        text-align:center;
                    }
                    
                    .visual-report.horizontal .car-stage {
                        position:relative;
                        width:300px;
                        height:160px;
                        text-align:center;
                    }
                    
                    .car-stage .car-shadow {
                         width: 0px;
                        position: absolute;
                        height: 100px;
                        left: 50%;
                        top: 171px;
                        z-index: 1;
                        box-shadow: 0 0 100px 80px rgba(0, 0, 0, 0.85);
                    }
                    
                    .visual-report.horizontal .car-shadow {
                       width: 20px;
                        position: absolute;
                        height: 10px;
                        left: 50%;
                        top: 93px;
                        z-index: 1;
                        box-shadow: 0 0 40px 40px rgba(0, 0, 0, 0.85);
                    }
                    
                    .car-stage .car {
                        margin-top:30px;
                        position:relative;
                        z-index:2;
                        max-width:100%;
                    }
                    
                    .visual-report.horizontal .car-stage .car {
                        margin-top:0;
                        position:relative;
                        z-index:2;
                        max-width:100%;
                    }
                    
                    .car-stage .part-point {
                       
                        position:absolute;
                        margin-left:-7.5px;
                        margin-top:-7.5px;
                        z-index:3;
                    }
                    
                    
                    
                    
                `}</style>

                <div className='car-stage'>
                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Front'} partReport={partResults.get("front")}/>
                    )}>
                        <PulsingDot
                            onClick={() => this.clickPart('front') }
                            className={'part-point '}
                            focused={this.props.focused === 'front'}
                            level={ partResults.get("front").hasDamage ? 'warn':''}
                            style={styleSet.front}/>
                    </Tooltip>


                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Rear'} partReport={partResults.get("rear")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('rear') }
                        className={'part-point'}
                        focused={this.props.focused === 'rear'}
                        level={ partResults.get("rear").hasDamage ? 'warn':''}
                        style={styleSet.rear}/></Tooltip>

                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Left Front'} partReport={partResults.get("leftFront")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('leftFront') }
                        className={'part-point'}
                        focused={this.props.focused === 'leftFront'}
                        level={ partResults.get("leftFront").hasDamage ? 'warn':''}
                        style={styleSet.leftFront}/></Tooltip>

                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Left Side'} partReport={partResults.get("leftSide")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('leftSide') }
                        className={'part-point'}
                        focused={this.props.focused === 'leftSide'}
                        level={ partResults.get("leftSide").hasDamage ? 'warn':''}
                        style={styleSet.leftSide}/></Tooltip>

                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Left Rear'} partReport={partResults.get("leftRear")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('leftRear') }
                        className={'part-point'}
                        focused={this.props.focused === 'leftRear'}
                        level={ partResults.get("leftRear").hasDamage ? 'warn':''}
                        style={styleSet.leftRear}/></Tooltip>

                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Right Front'} partReport={partResults.get("rightFront")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('rightFront') }
                        className={'part-point'}
                        focused={this.props.focused === 'rightFront'}
                        level={ partResults.get("rightFront").hasDamage ? 'warn':''}
                        style={styleSet.rightFront}/></Tooltip>

                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Right Side'} partReport={partResults.get("rightSide")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('rightSide') }
                        className={'part-point'}
                        focused={this.props.focused === 'rightSide'}
                        level={ partResults.get("rightSide").hasDamage ? 'warn':''}
                        style={styleSet.rightSide}/></Tooltip>

                    <Tooltip disable={!this.props.tooltip} offsetVertical={-10} offsetHorizontal={10} tooltipContents={(
                        <PartResultViewer title={'Right Rear'} partReport={partResults.get("rightRear")}/>
                    )}><PulsingDot
                        onClick={() => this.clickPart('rightRear') }
                        className={'part-point'}
                        focused={this.props.focused === 'rightRear'}
                        level={ partResults.get("rightRear").hasDamage ? 'warn':''}
                        style={styleSet.rightRear}/></Tooltip>

                    <img className='car' src={ this.props.horizontal ? '/images/car_horizontal.png' : '/images/KakaoTalk_20200113_193114366.png'}/>
                    <div className='car-shadow'/>
                </div>

            </div>
        )
    }
}