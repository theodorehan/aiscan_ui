import { Provider } from 'mobx-react'
import { getSnapshot } from 'mobx-state-tree'
import App from 'next/app'
import React from 'react'
import '@fortawesome/fontawesome-free/css/all.css'
import '../styles/reset.css'
import '../styles/global.css'
import { initializeStore, IStore } from '../stores/store'
import {PlanetShaperProvider} from "../contexts/PlanetShaperProvider";
import {ImageCakeProvider} from "../contexts/ImageCakeContext";

interface IOwnProps {
  isServer: boolean
  initialState: IStore
  mobile: boolean;
}

class MyApp extends App<IOwnProps> {
  public static async getInitialProps({ Component, router, ctx }) {
    //
    // Use getInitialProps as a step in the lifecycle when
    // we can initialize our store
    //
    const isServer = typeof window === 'undefined'
    const store = initializeStore(isServer)
    //
    // Check whether the page being rendered by the App has a
    // static getInitialProps method and if so call it
    //
    let pageProps = {
      query: router.query,
    }
    ctx.store = store;

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }


    let ua, mobile = false;
    if( ctx.req ){
      ua = ctx.req.headers['user-agent'];
    } else {
      ua = navigator.userAgent;
    }
    console.log(ua);

    if( ua.indexOf('Android') > -1 || ua.indexOf('iPhone') > -1){
      mobile = true;
    }


    return {
      initialState: getSnapshot(store),
      isServer,
      pageProps,
      mobile
    }
  }

  private store: IStore

  constructor(props) {
    super(props)
    this.store = initializeStore(props.isServer, props.initialState) as IStore
  }

  public render() {
    const { Component, pageProps } = this.props
    return (
      <Provider store={this.store}>
          <ImageCakeProvider>
            <PlanetShaperProvider>

                <Component {...pageProps} mobile={this.props.mobile}/>

            </PlanetShaperProvider>
          </ImageCakeProvider>
      </Provider>
    )
  }
}

export default MyApp
