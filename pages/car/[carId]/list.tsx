import React from 'react';
import Logo from "../../../components/Logo";
import {IStore} from "../../../stores/store";
import {inject, observer} from "mobx-react";
import {CarItem, RentDocument, RentDocumentType} from "../../../@types/data";
import Router, {withRouter} from 'next/router'
import Table from "../../../components/Table";
import moment from "moment";
import {actionToKorean, scanResultToImageUrl} from "../../../service/convertStrings";
import Label from "../../../components/Label";
import Image from "../../../components/Image";
import Pagenation from "../../../components/Pagenation";

class CarReportList extends React.Component<
    {
        mobile: boolean;
        store: IStore;
        query: {
            carId: string;
        }
    },

    {
        cars : Array<CarItem>;
        currentCar: CarItem;
        documents: Array<RentDocument>;
        currentDocumentPage: number;
    }
> {
    constructor(props) {
        super(props);

        this.state = {
            cars: [],
            currentCar: null,
            documents: [],
            currentDocumentPage:1,
        }
    }

    changePage(page){
        this.setState({
            currentDocumentPage:page,
        }, () => {
            this.getDocList()
        })
    }

    clickRow(row) {

        Router.push(`/car/${this.props.query.carId}/report?startDocumentSeq=${row.seq}` )
    }

    clickCar(car) {
        Router.push(`/car/${car.carNo}/list` )
    }

    getDocList = async () => {

        const documents = await this.props.store.fetch({
            path: `/document/list`,
            method: 'get',
            params: {
                carSeq: this.state.currentCar.seq,
                page: this.state.currentDocumentPage -1,
            }
        })


        this.setState({
            documents: documents.data || []
        })
    }

    loadCarList = async () => {

        const cars = await this.props.store.fetch({
            path: `/car/list`,
            method: 'get',
        })

        console.log(cars.data)

        if( cars.data ){
            if( Array.isArray(cars.data) ){
                this.setState({
                    cars: cars.data,
                }, () => {
                    console.log(this.props.query)
                    if( this.props.query.carId ){
                        const currentCar = this.state.cars.find((car:CarItem) => car.carNo === this.props.query.carId );
                        console.log(currentCar)
                        if( currentCar ){
                            this.setState({
                                currentCar: currentCar,
                            }, () => {

                                this.getDocList()
                            })
                        }
                    }
                })
            }
        }


    }

    selectCar(car:CarItem) : void {
        this.setState({currentCar:car});
    }


    componentDidMount(): void {
        this.loadCarList();
    }

    renderDocList(){
        return <div className='docs'>
            <style jsx>{`
                .date {
                    font-size:14px;
                    color:#fff;
                    padding:5px;
                }
                
                .action {
                    text-align:center;
                }
                
                .images {
                    vertical-align:top;
                }
                
                .pagenation {
                   text-align:center;
                }
            `}</style>

            <Table columns={[
                {
                    title: '번호',
                    key: 'seq',
                },
                {
                    title: '사진',
                    key:'scanResults',
                    render: (row,colKey,features) => (
                        <div className={'images'}>
                            { row[colKey].slice(0,this.props.mobile ? 2: 10).map((ret,i) => (
                                <Image key={i} width={20} url={scanResultToImageUrl(ret, row.seq)} style={{marginRight:5}}/>
                            ))}
                        </div>
                    )
                },
                {
                    title: '날짜',
                    key: 'datetime',
                    render: (row,colKey,features) => (
                        <div className={'date'}>
                            { this.props.mobile ? moment(row[colKey]).local().format("YY/MM/DD h:mm A") : moment(row[colKey]).local().format("YYYY년 MM월 DD일 h:mm A") }
                        </div>
                    )
                },

                {
                    title: '상태',
                    key: 'action',
                    render: (row,colKey,features) => (
                        <div className={'action'}>
                            <Label>{ actionToKorean(row[colKey]) }</Label>
                        </div>
                    )
                },

                {
                    title: '연결',
                    key: 'link',
                    width: 40,
                    rowSpan: ( row, key, rowIndex ) => {
                        if( row.action === 'return' ){
                            const prevRow = this.state.documents[rowIndex + 1];
                            if( prevRow && prevRow.action === 'rent' ){
                                return 2;
                            }
                        }

                        return 1;
                    },
                    skipTest: (row, key, rowIndex) => {
                        if( row.action === 'rent' ){
                            const nextRow = this.state.documents[rowIndex - 1];
                            if( nextRow && nextRow.action === 'return' ){
                                return true;
                            }
                        }

                        return false;
                    },
                    render: (row,colKey,features) => (
                        <div className={'link'}>
                            <i className='fa fa-link'/>
                        </div>
                    )
                }
            ]}

                   rows={this.state.documents}
                onClickRow={(row, rowIndex) => {
                    if( row.action === 'rent' ){

                        this.clickRow(row)
                    } else if ( row.action === 'return' ){
                        let prevRow = this.state.documents[rowIndex +1];
                        if( prevRow && prevRow.action === 'rent' ){
                            this.clickRow(prevRow);
                        } else {

                            this.clickRow(row)
                        }
                    }

                }}/>

                <div className='pagenation'>

                    <Pagenation
                        pageCount={199}
                        currentPage={this.state.currentDocumentPage}
                        pageMarkableCount={10}
                        onMovePage={(page:number) => this.changePage(page)}/>
                </div>
        </div>
    }

    renderCarList(){
        return <div>
            <style jsx>{`
                .mobile-car-list {
                    padding:10px 0; 
                }
            `}</style>

            { this.props.mobile ? (
                <div className={'mobile-car-list'}>
                    {
                        this.state.cars.map((car,i) => (
                            <Label style={{marginRight:4}} key={i} onClick={() => this.clickCar(car)}>
                                { car.carNo }
                            </Label>
                        ))
                    }
                </div>
            ) : (
                <Table columns={[
                    {
                        title: '차 번호',
                        key: 'carNo',
                        render(row, key, features){
                            return (
                                <Label>
                                    {row[key]}
                                </Label>
                            )
                        }
                    }
                ]}
                       rows={this.state.cars}
                onClickRow={(row) => this.clickCar(row)}/>
            )}
        </div>
    }

    render(){
        return (
            <div className={ 'wrapper ' +  (this.props.mobile ? 'mobile':'pc')}>
                <style jsx>{`
                    .wrapper.mobile {
                        
                    }
                    .wrapper.mobile .contents {
                        width:100%;
                    }
                    
                    .wrapper.pc .contents {
                        width:1280px;
                        margin:auto;
                    }
                    
                    .wrapper.pc .chart {
                        display:flex;
                    }
                    
                    .wrapper.pc .car-list {
                        width:200px;
                        margin-right:20px;
                    }
                    
                    .wrapper.pc .doc-list {
                        flex:1;
                    }
                    
                     .wrapper.mobile .chart {
                        display:block;
                    }
                    
                    .wrapper.mobile .car-list {
                        width:200px;
                    }
                    
                    .wrapper.mobile .doc-list {
                        flex:1;
                    }
                `}</style>


                <div className='contents'>
                    <header className='header'>
                        <Logo/>
                    </header>

                    <div className={'chart'}>
                        <div className='car-list'>

                            { this.renderCarList() }

                        </div>

                        <div className='doc-list'>
                            { this.renderDocList() }
                        </div>
                    </div>

                </div>



                <footer>


                    aiscans (c) 2020

                </footer>
            </div>
        )
    }
}

export default inject('store')(observer(CarReportList));