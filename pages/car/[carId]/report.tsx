import React from "react"
import moment from 'moment';
import _ from 'lodash';

import Logo from "../../../components/Logo";
import config from '../../../config';

import Router, {withRouter} from 'next/router'
import VisualReport from "../../../components/VisualReport";
import GoogleMap from '../../../components/GoogleMap';
//

import {inject, observer} from 'mobx-react'
import {IStore} from "../../../stores/store";
import {PartReport, ScanResult, SurfaceTypes, RentDocumentType, RentDocument} from "../../../@types/data";
import Table from "../../../components/Table";
import {DamageNumberMap, DamageProcess, DamageSymptom} from "../../../constants/damage";
import Toggle from "../../../components/Toggle";
import PlanetShaperButton from "../../../components/Button";
import {PlanetShaperContext} from "../../../contexts/PlanetShaperProvider";
import Image from "../../../components/Image";
import {actionToKorean, scanResultToImageUrl} from "../../../service/convertStrings";


class IndexPage extends React.Component<{
    carItem: {
        carNo: number,
        seq: number,
    };
    query: {
        startDocumentSeq: number;
    };
    store: IStore;
    mobile: boolean;
}, {
    rentAndReturnDocs: Array<RentDocument>;
    focusedPartReport: PartReport | null;
    focusedPart: SurfaceTypes | null;
    focusedDoc: RentDocumentType | null;
    viewReturn: boolean;
}> {

    static contextType = PlanetShaperContext;

    static async getInitialProps(ctx) {
        const carItem = await ctx.store.fetch({
            path: `/car/${ctx.query.carId}/read`,
            method: 'get',
        })

        let ua, mobile = false;
        if( ctx.req ){
            ua = ctx.req.headers['user-agent'];
        } else {
            ua = navigator.userAgent;
        }

        if( ua.indexOf('Android') > 0 || ua.indexOf('iPhone') > 0){
            mobile = true;
        }


        return {
            carItem: carItem.data || null,
            rentAndReturnDocs: [],
            mobile,
            query: ctx.query
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            rentAndReturnDocs: [],
            focusedPartReport: null,
            focusedPart: null,
            focusedDoc: null,

            viewReturn: false,
        }
    }

    loadLastTwoDocs = async () => {
        if (this.props.carItem) {
            const carItem = await this.props.store.fetch({
                path: `/car/${this.props.carItem.seq}/lastTwoDocuments`,
                method: 'get',
                params: {
                    startAt: this.props.query.startDocumentSeq
                }
            })


            let docs = carItem.data;
            if( !docs ) return;



            let lastRentActedIndex = -1;
            for (let i = 0; i < docs.length; i++) {
                let doc = docs[i];
                if (doc.action === 'rent') {
                    lastRentActedIndex = i;
                }
            }

            let pair = docs.slice(lastRentActedIndex, 2);
            if( docs[0] && docs[0].action  === 'return'){
                /**
                 * if first document of two documents is started at 'return' status that's broken pair. therefore report will show only the one first document.
                 **/
                pair = [docs[0]];
            }
            if (pair[0]) {
                const scanResult = await this.props.store.fetch({
                    path: `/document/${pair[0].seq}/scanResult`,
                    method: 'get',
                })

                if( scanResult.data ){

                    pair[0].scanResult = scanResult.data;
                }

            }

            if (pair[1]) {
                const scanResult = await this.props.store.fetch({
                    path: `/document/${pair[1].seq}/scanResult`,
                    method: 'get',
                })

                if( scanResult.data ){

                    pair[1].scanResult = scanResult.data;
                }


            }

            this.setState({
                rentAndReturnDocs: pair,
            })
        }

    }

    openMobileMap = () => {
        this.context.actions.openModal({
            component: GoogleMap,
            title: 'Map',
            props: {
                apiKey:'AIzaSyAkmeIplRQiM9qIL_bxL1kVxrA8z5PvpyA',
                startAt: this.getGoogleCoordinates(),
                width:'100vw',
                height:'calc(100vh - 60px)'
            },

        })
    }

    showPartDetails(docType: RentDocumentType, location: SurfaceTypes, report: PartReport ){
        this.setState({
            focusedDoc: docType,
            focusedPart: location,
            focusedPartReport: report,
        }, () => {
            if( this.props.mobile ){

                if( this.state.focusedPartReport ){
                    this.context.actions.openModal({

                        component : () => (
                            <Table columns={
                                [
                                    {key:'$expand', title: 'Expand'},
                                    {key:'code', title: 'Code'},
                                    {key:'status', title: 'Status'},
                                    {key:'symptom', title: '증상'},
                                    {key:'process', title: '공정'},
                                ]}
                                   rows={ this.getRowData() }
                                   renderExpansion={this.renderDamageExpansion}/>
                        ),

                        props: {},
                        title: 'Detail',


                    })
                }
            }
        })

    }

    getRowData(){

        const keys = Object.keys(DamageNumberMap);
        console.log(keys);


        return keys.map((k) => ({
            code: DamageNumberMap[k],
            status: this.state.focusedPartReport.damageMap.get(parseInt(k)) ? <div style={{color:'red'}}> 해당 </div>:"해당없음",
            symptom: DamageSymptom[k],
            process: DamageProcess[k],
        }))
    }

    getGoogleCoordinates(){
        if( this.state.rentAndReturnDocs[0] ){
            let lat, lng;
            const dmsLat = this.state.rentAndReturnDocs[0].lat;

            const dmsLng = this.state.rentAndReturnDocs[0].lng;

            if( dmsLat ){
                let [degree, min, sec] = dmsLat.split(',');

                lat = parseFloat(degree) + ( parseFloat(min) / 60 ) + ( parseFloat(sec) / 3600 );
            }

            if( dmsLng ){
                let [degree, min, sec] = dmsLng.split(',');

                lng = parseFloat(degree) + ( parseFloat(min) / 60 ) + ( parseFloat(sec) / 3600 );
            }

            if( lat && lng ){
                return `${lat},${lng}`;
            }
        }

        return undefined;
    }

    componentDidMount(): void {
        this.loadLastTwoDocs();
    }

    renderPartPictures(){
        let targetDocId;
        const docType = this.state.focusedDoc;
        if(docType === "rent"){
            targetDocId = this.state.rentAndReturnDocs[0].seq
        } else if ( docType === "return" ){
            targetDocId = this.state.rentAndReturnDocs[1].seq
        }

        return (
            <div>
                {this.state.focusedPartReport.rawPartScans.map((scan:ScanResult) => (
                    <Image
                        width={40}
                        group={'parts'+this.state.focusedPart}
                        url={scanResultToImageUrl(scan, targetDocId) }
                        key={scan.fileIndex}
                        style={{maxWidth:40}} />
                ))}
            </div>
        )
    }

    renderDamageExpansion = (row, index) => {

        const damageScans = this.state.focusedPartReport.damageMap.get(index+1) || [];
        const docType = this.state.focusedDoc;
        let targetDocId;
        if(docType === "rent"){
            targetDocId = this.state.rentAndReturnDocs[0].seq
        } else if ( docType === "return" ){
            targetDocId = this.state.rentAndReturnDocs[1].seq
        }

        return (
            <div>
                {damageScans.map((scan:ScanResult) => (
                    <Image
                        group={'damaged'+index}
                        key={scan.fileIndex}
                        style={{maxWidth:40}}
                        url={scanResultToImageUrl(scan, targetDocId)}/>
                ))}
            </div>
        )
    }

    renderDocumentInfo(documentInfo) {


        if (this.props.carItem && documentInfo) {
            return (
                <ul>
                    <style jsx>{`
                    
                    ul {
                        background-color:rgba(0,0,0,.5);
                        padding:5px;
                        margin:5px;
                    }
           .label {
              display:inline-block; 
              width:90px;
           }
           .value {
              display:inline-block;
           }
          `}</style>
                    <li>
                        <div className='label'>
                            기록번호
                        </div>
                        <div className='value'>
                            {documentInfo.seq}
                        </div>
                    </li>

                    <li>
                        <div className='label'>
                            자동차번호
                        </div>
                        <div className='value'>
                            {this.props.carItem.carNo}
                            ({this.props.carItem.seq})
                        </div>
                    </li>

                    <li>
                        <div className='label'>
                            상태
                        </div>
                        <div className='value'>
                            { actionToKorean(documentInfo.action)  }
                        </div>
                    </li>

                    <li>
                        <div className='label'>
                            날짜
                        </div>
                        <div className='value'>
                            { moment(documentInfo.datetime).format("YYYY년 MM월 DD일 h:mm A") }
                        </div>
                    </li>

                </ul>
            )
        } else {
            return (
                <div>
                    Not Found
                </div>
            )
        }
    }

    renderMobile(){
        return (
            <div className={'wrapper'}>
                <style jsx>{`
                    .wrapper {
                        display:flex; 
                        flex-direction: column; 
                    }
                    
                    .info {
                        height:100%; 
                        color:#fff;
                    }
                    
                    .contents {
                        width:100%;
                        
                    }
                    
                    .contents .report {
                        margin:auto;
                        width:300px;  
                    }
                    
                    .footer { 
                        position: fixed;
                        z-index:10;
                        width: 100%;
                        height: 40px;
                        box-sizing: border-box;
                        bottom: 0;
                        padding: 5px;
                        background-color: #2d6ac6;
                    }
                    
                    .footer .current-document {
                        display:inline-block;
                        color:#fff;
                        
                    }
                    
                    .footer > * {
                        display:inline-block;
                        vertical-align:middle;
                        margin-right:10px;
                     }
                `}</style>

                <header className='header'>
                    <Logo/>
                </header>

                <div className={'info'}>
                    {this.renderDocumentInfo(this.state.rentAndReturnDocs[0])}
                </div>

                <div className='contents'>
                    <div className='report'>
                        {

                            this.state.rentAndReturnDocs[0] && (
                                <VisualReport
                                    horizontal
                                    tooltip={false}
                                    focused={this.state.focusedDoc === "rent" ? this.state.focusedPart : undefined}
                                    onClickLocation={(location, report) => this.showPartDetails('rent', location, report)}
                                    scanResults={this.state.rentAndReturnDocs[0].scanResult}
                                />
                            )
                        }
                    </div>
                </div>

                <div className={'info'}>
                    {this.renderDocumentInfo(this.state.rentAndReturnDocs[1])}
                </div>
                <div className='contents'>
                    <div className='report'>
                        {
                            this.state.rentAndReturnDocs[1] ? (
                                <VisualReport
                                    horizontal
                                    tooltip={false}
                                    focused={this.state.focusedDoc === "rent" ? this.state.focusedPart : undefined}
                                    onClickLocation={(location, report) => this.showPartDetails('return', location, report)}
                                    scanResults={this.state.rentAndReturnDocs[0].scanResult }
                                />
                            ) : ("아직 반납 결과가 없습니다")

                        }
                    </div>



                </div>

                <div>


                </div>



                <div className={'footer'}>
                    <div>
                        <PlanetShaperButton onClick={this.openMobileMap} >
                            지도보기
                        </PlanetShaperButton>
                    </div>

                    <div>
                        <PlanetShaperButton onClick={() => Router.push(`/car/${this.props.carItem.carNo}/list`)} >
                            목록으로
                        </PlanetShaperButton>
                    </div>
                </div>
            </div>
        )
    }


    render() {
        if( this.props.mobile ){
            return this.renderMobile();
        }


        let firstImageRow = {};
        let secondImageRow = {};
        if( this.state.rentAndReturnDocs[0] ){
            const doc = this.state.rentAndReturnDocs[0];
            const groups = _.groupBy(doc.scanResult, (ret) => ret.direction);

            firstImageRow = _.mapValues(groups, (value = [], key) => {
                console.log(value);
                return value.map((item: ScanResult, i) => (
                    <Image
                        group={`summary-images-${key}`}
                        key={`${key}-${i}`}
                        style={{maxWidth:40}}
                        url={scanResultToImageUrl(item, doc.seq)}/>
                ))
            })
        }

        if( this.state.rentAndReturnDocs[1] ){
            const doc = this.state.rentAndReturnDocs[1];
            const groups = _.groupBy(doc.scanResult, (ret) => ret.direction);

            secondImageRow = _.mapValues(groups, (value = [], key) => {
                console.log(value);
                return value.map((item: ScanResult, i) => (
                    <Image
                        group={`summary-images-${key}`}
                        key={`${key}-${i}`}
                        style={{maxWidth:40}}
                        url={scanResultToImageUrl(item, doc.seq)}/>
                ))
            })
        }

        return (
            <div className='wrapper white'>
                <style jsx>{`
                .wrapper {
                    background-color:#fff;
                }
            .report {
              text-align:center; 
              width:100%;
                
              box-sizing:border-box;
              display:flex;
            }
            .report .split {
              display:inline-block;
              width:400px;
              text-align:center;
               flex:1;
            }
            
            .report .split .title { 
              padding: 10px; 
              letter-spacing: 10px;
              text-transform: uppercase;
              text-shadow: 0 0 15px #a5a5a5;
              color: #333;
              font-size: 24px;
            }
            
           .report .split .table-title { 
              padding: 10px; 
              letter-spacing: 10px;
              text-transform: uppercase;
              text-shadow: 0 0 15px #a5a5a5;
              color: #333;
              font-size: 24px; 
            }
            
            .information {
              position:relative;
              width:100%;
              height:300px;
            }
            
            .information .overlay-info {
                display:inline-block;
                width: 300px;   
                height: 100%; 
                color:#fff;
                font-size:14px; 
                box-sizing:border-box;
                line-height:120%; 
                margin-right:80px;
            }
            
            .overlay-info .label {
              display:inline-block; 
              width:100px;
            }
            .overlay-info .value {
              display:inline-block;
            }
            
            .information .map-area {
                display:inline-block;
                width:900px;
                height: 100%;
                vertical-align:top;
            }
            
            .pictures {
                display:flex;
            }
            
            .pictures .split {
                flex:1;
            }
            
            .pictures .split {
               // border-right:2px solid #777;
               padding:10px;
            }
            
            .pictures .split:last-child {
                border:0;
            }
            
            .pictures .split .title {
                  padding: 10px; 
                  letter-spacing: 10px;
                  text-transform: uppercase;
                  text-shadow: 0 0 15px #a5a5a5;
                  color: #333;
                  font-size: 24px; 
                  text-align:center;
            }
            
            .footer {
                color:#333;
            }
          `}</style>
                <div className='contents'>
                    <header className='header'>
                        <Logo theme={'dark'}/>

                        <div className={'menus'}>
                            <PlanetShaperButton onClick={() => Router.push(`/car/${this.props.carItem.carNo}/list`)} >
                                목록 보기
                            </PlanetShaperButton>
                        </div>

                    </header>

                    <main>
                        <div className='information'>

                            <div className='overlay-info'>
                                {this.renderDocumentInfo(this.state.rentAndReturnDocs[0])}



                                {this.renderDocumentInfo(this.state.rentAndReturnDocs[1])}

                              {/*  <i className={'fa fa-chevron-left'}/>*/}
                              {/*-*/}
                              {/*  <i className={'fa fa-chevron-right'}/>*/}
                            </div>
                            {/*// planet4.theo@google.com's api key*/}
                            <div className='map-area'>
                                <GoogleMap
                                    apiKey='AIzaSyAkmeIplRQiM9qIL_bxL1kVxrA8z5PvpyA'
                                    startAt={ this.getGoogleCoordinates() }/>
                            </div>
                        </div>

                        <div className='report'>
                            {
                                this.state.focusedPartReport && (
                                    <div className='split'>
                                       <div className={"table-title"}>{this.state.focusedPart}</div>

                                        <Table columns={
                                            [
                                                {key:'$expand', title: 'Expand'},
                                                {key:'code', title: 'Code'},
                                                {key:'status', title: 'Status'},
                                                {key:'symptom', title: '증상'},
                                                {key:'process', title: '공정'},
                                            ]}
                                               theme={'white'}
                                               rows={ this.getRowData() }
                                               renderExpansion={this.renderDamageExpansion}/>


                                    </div>
                                )
                            }

                            {
                                this.state.rentAndReturnDocs[0] && (
                                    <div className={'split'}>
                                        <div className={'title'}>
                                            {actionToKorean(this.state.rentAndReturnDocs[0].action)}
                                        </div>
                                        <VisualReport
                                            focused={this.state.focusedDoc === "rent" ? this.state.focusedPart : undefined}
                                            onClickLocation={(location, report) => this.showPartDetails('rent', location, report)}
                                            scanResults={this.state.rentAndReturnDocs[0].scanResult }
                                        />
                                    </div>
                                )
                            }

                            {
                                this.state.rentAndReturnDocs[1] && (
                                    <div className={'split'}>
                                        <div className={'title'}>
                                            {actionToKorean(this.state.rentAndReturnDocs[1].action)}
                                        </div>
                                        <VisualReport
                                            focused={this.state.focusedDoc === "return" ? this.state.focusedPart : undefined}
                                            onClickLocation={(location, report) => this.showPartDetails('return', location, report)}
                                            scanResults={this.state.rentAndReturnDocs[1].scanResult }
                                        />
                                    </div>
                                )
                            }

                        </div>

                        <div className='pictures'>
                            {
                                this.state.rentAndReturnDocs[0] && (
                                    <div className='split'>

                                        <div className='title'>
                                            {actionToKorean(this.state.rentAndReturnDocs[0].action)}
                                        </div>
                                        <Table columns={
                                            [
                                                {key:'front', title: '정면(FRONT)'},
                                                {key:'leftFront', title: '좌측 모서리(LEFT FRONT)'},
                                                {key:'leftSide', title: '좌측 옆면(LEFT SIDE)'},
                                                {key:'leftRear', title: '좌측 후면 모서리(LEFT REAR)'},
                                                {key:'rear', title: '후면(REAR)'},
                                                {key:'rightRear', title: '우측 후면 모서리(RIGHT REAR)'},
                                                {key:'rightSide', title: '우측 옆면(RIGHT SIDE)'},
                                                {key:'rightFront', title: '우측 모서리(RIGHT FRONT)'},
                                                {key:'dashboard', title: '계기판'},
                                            ]}
                                               theme={'white'}
                                               horizontalTable
                                               rows={ [firstImageRow] }
                                               renderExpansion={this.renderDamageExpansion}/>
                                    </div>
                                )
                            }


                            {
                                this.state.rentAndReturnDocs[1] && (
                                    <div  className='split'>
                                        <div className='title'>
                                            {actionToKorean(this.state.rentAndReturnDocs[1].action)}
                                        </div>
                                        <Table columns={
                                            [
                                                {key:'front', title: '정면(FRONT)'},
                                                {key:'leftFront', title: '좌측 모서리(LEFT FRONT)'},
                                                {key:'leftSide', title: '좌측 옆면(LEFT SIDE)'},
                                                {key:'leftRear', title: '좌측 후면 모서리(LEFT REAR)'},
                                                {key:'rear', title: '후면(REAR)'},
                                                {key:'rightRear', title: '우측 후면 모서리(RIGHT REAR)'},
                                                {key:'rightSide', title: '우측 옆면(RIGHT SIDE)'},
                                                {key:'rightFront', title: '우측 모서리(RIGHT FRONT)'},
                                                {key:'dashboard', title: '계기판'},
                                            ]}
                                               theme={'white'}
                                               horizontalTable
                                               rows={ [secondImageRow] }
                                               renderExpansion={this.renderDamageExpansion}/>
                                    </div>
                                )
                            }

                        </div>
                    </main>

                    <footer>


                        aiscans (c) 2020

                    </footer>
                </div>
            </div>
        )
    }

}

export default inject('store')(observer(IndexPage));
