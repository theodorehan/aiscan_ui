import {RentDocumentType, ScanResult} from "../@types/data";
import config from '../config';

export function actionToKorean(type: RentDocumentType){
    if( type === "return" ){
        return '반납';
    } else if ( type === "rent" ){
        return "배차"
    }
}


export function scanResultToImageUrl(ret:ScanResult, docId:number) {
    return `${config.apiHost}/store/${ret.direction}/pic-${docId}-${ret.fileIndex}.jpg`
}