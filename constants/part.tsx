export const PartKoName : { [part:string] : string } = {
'front':'정면촬영',
'rightFront': '우측모서리',
'rightRear': '후면 우측 모서리',
'rightSide': '우측 옆면',
'leftRear': '후면 좌측 모서리',
'leftSide': '좌측 옆면',
'leftFront': '좌측 모서리',
'rear': '후면중앙',
}