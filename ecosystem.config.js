module.exports = {
    apps : [{
        name: "aiscan-ui",
        script: "./start.js",
        args: "start",
        env: {
            NODE_ENV: "production",
            CONFIG:'dev',
        },
        env_dev: {
            NODE_ENV: "production",
            CONFIG:'dev',
        }
    }]
}